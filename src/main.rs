use std::fs;
use atoi::atoi;
use std::env;
use std::collections::BinaryHeap;
use std::cmp::Ordering;

struct Elf{
    total: usize,
    food: Vec<usize>,
}

impl PartialOrd for Elf{
    fn partial_cmp(&self, other: &Self) -> Option<Ordering>{
	Some(self.total.cmp(&other.total))
    }
}

impl Ord for Elf{
    fn cmp(&self, other: &Self) -> Ordering{
	self.total.cmp(&other.total)
    }
}

impl PartialEq for Elf{
    fn eq(&self, other: &Self) -> bool{
	self.total ==  other.total
    }
}

impl Eq for Elf{
}

fn load_calories(filename: String) -> std::collections::BinaryHeap<Elf>{
    let mut elf_list:std::collections::BinaryHeap<Elf> = BinaryHeap::new();
    
    let contents = fs::read_to_string(filename)
        .expect("Should have been able to read the file");
    let mut pos = 0;
    loop{
	let mut elf = Elf{total:0, food:Vec::new()};
	'inner:loop{
	    let con_left = &contents[pos..];
	    	    match con_left.find('\n') {
		Some(next_eol) => {pos += next_eol+1},
		None => return elf_list,
	    }
	    match atoi::<usize>(con_left.as_bytes()){
		Some(num) =>{elf.total += num;
			     elf.food.push(num)},
		None => break 'inner}
	}
	elf_list.push(elf);
    }
}

fn solution1(filename: String) -> usize{
    let mut res = load_calories(filename);
    return res.pop().unwrap().total;
}

fn solution2(filename: String) -> usize{
    let mut res = load_calories(filename);
    let mut top_3 = 0;
    for _i in 0..3{
	top_3 += res.pop().unwrap().total;
    }
    return top_3;
}

fn lose(op: u8) -> u8{
    match op{
	65 => return 3,
	66 => return 1,
	67 => return 2,
	_ => panic!("invalid value"),
	
    }
}

fn win(op: u8) -> u8{
    match op{
	65 => return 2,
	66 => return 3,
	67 => return 1,
	_ => panic!("invalid value"),

    }
}


fn round_score(line :&[u8]) -> usize{
    let mut score = line[2] - 87;
    match line[2] - line[0]{
	23 => score += 3,
	24 => score += 6,
	21 => score += 6,	
	_ => score += 0
    }
    return score.into()
}

fn round_score2(line :&[u8]) -> usize{
    let mut score = 0;
    match line[2]{
	88 => score += 0 + lose(line[0]),
	89 => score += 3 + line[0]-64,
	90 => score += 6 + win(line[0]),	
	_ => score += 0
    }
    return score.into()
}


fn calculate_tournament(filename: String) -> usize{ 
    let contents = fs::read_to_string(filename)
        .expect("Should have been able to read the file");
    let mut pos = 0;
    let mut score = round_score(contents[pos..pos+3].as_bytes());
    loop{
	let con_left = &contents[pos..];
	match con_left.find('\n') {
	    Some(next_eol) => {pos += next_eol+1;
			       if pos < contents.len(){
				   score += round_score(contents[pos..pos+3].as_bytes());
			       }},
	    None => return score,
	    }
    }
}

fn calculate_tournament2(filename: String) -> usize{ 
    let contents = fs::read_to_string(filename)
        .expect("Should have been able to read the file");
    let mut pos = 0;
    let mut score = round_score2(contents[pos..pos+3].as_bytes());
    loop{
	let con_left = &contents[pos..];
	match con_left.find('\n') {
	    Some(next_eol) => {pos += next_eol+1;
			       if pos < contents.len(){
				   score += round_score2(contents[pos..pos+3].as_bytes());
			       }},
	    None => return score,
	    }
    }
}


fn get_dup_priority(line: &str)-> usize{
    let first = &line[..((line.len())/2)];
    let second = &line[(line.len())/2..];
    let mut num:usize = 0;
    for letter in first.as_bytes(){
	match second
	    .as_bytes()
            .iter()
            .position(|&x| x == *letter){
		Some(_) => {if *letter >= 97 {
		    num += *letter as usize - 96;
		}
			    else {
				num += *letter as usize - 65 + 27;
			    }
			    return num
		}
		_ => ()
	}
    }
    return num
}

fn get_wrong_sacks(filename: String) -> usize{
    let contents = fs::read_to_string(filename)
        .expect("Should have been able to read the file");
    let mut priority = 0;
    for line in contents.lines(){
	priority += get_dup_priority(line);
    }
    return priority
}


fn get_badge_priority(line1: &str, line2: &str, line3: &str)-> usize{
    for letter in line1.as_bytes(){
	match line2
	    .as_bytes()
            .iter()
            .position(|&x| x == *letter){
		Some(_) => {match line3
			    .as_bytes()
			    .iter()
			    .position(|&x| x == *letter){		
				Some(_) => {
				    if *letter >= 97 {
					return *letter as usize - 96;
				    }
				    else {
					return *letter as usize - 65 + 27;
				    }
				}
				_ => ()
			    }
		},
		_ => ()
		    
	    }
    }
    return 2137
}

fn get_badges(filename: String) -> usize{
    let contents = fs::read_to_string(filename)
        .expect("Should have been able to read the file");
    let mut priority = 0;
    let mut i = 0;
    let mut line1 = "";
    let mut line2 = "";
    let mut line3;

    for line in contents.lines(){
	match i{
	    0 => {line1 = line;
		  i+=1},
	    1 => {line2 = line;
		  i+=1},
	    2 => {line3 = line;
		  i=0;
		  priority += get_badge_priority(line1, line2, line3);
	    },
	    _ => panic!("SOmething wrong"),
	}
    }
    return priority
}

fn check_overlap(line: &str)-> usize{
    let lstart = atoi::<usize>(line.as_bytes()).unwrap();
    let mut pos = line.chars().position(|c| c == '-').unwrap()+1;
    let lend = atoi::<usize>(line[pos..].as_bytes()).unwrap();
    pos += line[pos..].chars().position(|c| c == ',').unwrap()+1;
    let rstart = atoi::<usize>(line[pos..].as_bytes()).unwrap();
    pos += line[pos..].chars().position(|c| c == '-').unwrap()+1;
    let rend = atoi::<usize>(line[pos..].as_bytes()).unwrap();
    if lstart <= rstart && lend >= rend {
	return 1;
    }
    if rstart <= lstart && rend >= lend {
	return 1;
    }
    
    return 2137;
}

fn check_overlap2(line: &str)-> usize{
    let lstart = atoi::<usize>(line.as_bytes()).unwrap();
    let mut pos = line.chars().position(|c| c == '-').unwrap()+1;
    let lend = atoi::<usize>(line[pos..].as_bytes()).unwrap();
    pos += line[pos..].chars().position(|c| c == ',').unwrap()+1;
    let rstart = atoi::<usize>(line[pos..].as_bytes()).unwrap();
    pos += line[pos..].chars().position(|c| c == '-').unwrap()+1;
    let rend = atoi::<usize>(line[pos..].as_bytes()).unwrap();
    if lstart <= rstart && rstart <= lend {
	return 1;
    }
    if rstart <= lstart && lstart <= rend {
	return 1;
    }
    
    return 2137;
}


fn get_overlap(filename: String) -> usize{
    let contents = fs::read_to_string(filename)
        .expect("Should have been able to read the file");
    let mut duplicates = 0;

    for line in contents.lines(){
	duplicates += check_overlap(line);
    }
    return duplicates
}

fn get_overlap2(filename: String) -> usize{
    let contents = fs::read_to_string(filename)
        .expect("Should have been able to read the file");
    let mut duplicates = 0;

    for line in contents.lines(){
	duplicates += check_overlap2(line);
    }
    return duplicates
}

fn rearrange(filename: String) -> usize{
    let contents = fs::read_to_string(filename)
        .expect("Should have been able to read the file");
    let mut stacks: Vec<Vec<char>> = vec![Vec::new(); 9];

    let mut state = 0;
    for line in contents.lines(){
	match state{
	    0 => {
		if line.as_bytes()[1] == 49{
		    state = 1;
		    continue;
		}
		for i in 1..line.len(){
		let some_char = line.as_bytes()[i];
		if some_char >= 65 && some_char <= 90{
		    stacks[(i-1)/4].insert(0, line.chars().nth(i).unwrap());
		    println!("{} {}", some_char, i);
		}
	    }
	    }
	    1 => {
		if line == ""{
		    ()
		}
		else{
		    println!("{}", line);
		    let pos = line.chars().position(|c| c == 'e').unwrap()+2;
		    let num = atoi::<usize>(line[pos..].as_bytes()).unwrap();
		    let pos = pos + line[pos..].chars().position(|c| c == 'm').unwrap()+2;
		    let frm = atoi::<usize>(line[pos..].as_bytes()).unwrap();
		    let pos = pos + line[pos..].chars().position(|c| c == 'o').unwrap()+2;
		    let to = atoi::<usize>(line[pos..].as_bytes()).unwrap();
		    for _ in 0..num{
			let item = stacks[frm-1].pop().unwrap();
			stacks[to-1].push(item);
		    }
		}
		
	    }
	    _ => ()
	}
    }
    
    for i in 0..9{
	println!("{}", stacks[i].pop().unwrap());
    }

    return 2137;
}

fn rearrange2(filename: String) -> usize{
    let contents = fs::read_to_string(filename)
        .expect("Should have been able to read the file");
    let mut stacks: Vec<Vec<char>> = vec![Vec::new(); 9];

    let mut state = 0;
    for line in contents.lines(){
	match state{
	    0 => {
		if line.as_bytes()[1] == 49{
		    state = 1;
		    continue;
		}
		for i in 1..line.len(){
		let some_char = line.as_bytes()[i];
		if some_char >= 65 && some_char <= 90{
		    stacks[(i-1)/4].insert(0, line.chars().nth(i).unwrap());
		    println!("{} {}", some_char, i);
		}
	    }
	    }
	    1 => {
		if line == ""{
		    ()
		}
		else{
		    println!("{}", line);
		    let pos = line.chars().position(|c| c == 'e').unwrap()+2;
		    let num = atoi::<usize>(line[pos..].as_bytes()).unwrap();
		    let pos = pos + line[pos..].chars().position(|c| c == 'm').unwrap()+2;
		    let frm = atoi::<usize>(line[pos..].as_bytes()).unwrap();
		    let pos = pos + line[pos..].chars().position(|c| c == 'o').unwrap()+2;
		    let to = atoi::<usize>(line[pos..].as_bytes()).unwrap();

		    let mut tmp_stack: Vec<char> = Vec::new();
		    
		    for _ in 0..num{
			let item = stacks[frm-1].pop().unwrap();
			tmp_stack.push(item);
		    }
		    for _ in 0..num{
			let item = tmp_stack.pop().unwrap();
			stacks[to-1].push(item);
		    }
		    
		}
		
	    }
	    _ => ()
	}
    }
    
    for i in 0..9{
	println!("{}", stacks[i].pop().unwrap());
    }

    return 2137;
}

fn start_of_transfer(filename: String) -> usize{
    let contents = fs::read_to_string(filename)
        .expect("Should have been able to read the file");
    for line in contents.lines(){
	for i in 4..line.len(){
	    let bytes_slice = &line.as_bytes()[i-3..i];
	    for j in 0..3{
		let pos = bytes_slice[j+1..].iter().position(|c| *c == bytes_slice[j]);
		match pos{
		    Some(_) => break,
		    None => if j==2{return i},
		}
	    }
	}
    }


    return 2137;
}

fn start_of_msg(filename: String) -> usize{
    let contents = fs::read_to_string(filename)
        .expect("Should have been able to read the file");
    for line in contents.lines(){
	for i in 14..line.len(){
	    let bytes_slice = &line.as_bytes()[i-14..i];
	    for j in 0..13{
		let pos = bytes_slice[j+1..].iter().position(|c| *c == bytes_slice[j]);
		match pos{
		    Some(_) => break,
		    None => if j==12{return i},
		}
	    }
	}
    }


    return 2137;
}

struct ElfixFile{
    name:String,
    size:usize,
    isdir: bool,    
    parent:Option<ElfixFile>,
    sub: Option<Vec<ElfixFile>>
}

fn elfix_os_filesystem(filename: String) -> usize{
    let contents = fs::read_to_string(filename)
        .expect("Should have been able to read the file");
    let mut depth = 0;
    let mut pwd = ElfixFile{
	name: String::from("/"),
	size: 0,
	isdir: true,
	parent: None,
	sub: None
    };

    let root = &pwd;
    
    for line in contents.lines(){
	if line.as_bytes()[0] == 36{
	    if line.as_bytes()[2] == 99{
		if line.as_bytes()[5] == 46{
		    pwd = pwd.parent.unwrap();
		    println!("foo");
		}
		else {
		    match pwd.sub{
			Some(_) => panic!("did not expect to enter the same dir twice!"),
			None => {pwd.sub = Some(Vec::new());
				 let tmp = ElfixFile{
				     name: String::from(&line[5..]),
				     size: 0,
				     isdir: true,
				     parent: Some(&pwd),
				     sub: None
				 };
				 pwd.sub.unwrap().push(tmp);
				 pwd = tmp;
				 
			}
		    }
		    depth += 1;
		}
	    }	
	}
    }
    return 2137
}

fn main() {
    let mut solutions: Vec<fn(String) -> usize> = Vec::new();
    solutions.push(solution1);
    solutions.push(solution2);    
    solutions.push(calculate_tournament);
    solutions.push(calculate_tournament2);    
    solutions.push(get_wrong_sacks);
    solutions.push(get_badges);
    solutions.push(get_overlap);
    solutions.push(get_overlap2);
    solutions.push(rearrange);            
    solutions.push(rearrange2);
    solutions.push(start_of_transfer);    
    solutions.push(start_of_msg);
    solutions.push(elfix_os_filesystem);    
    for i in env::args().skip(1) {
	println!("Day {} part 1 solution is {}", i, solutions[2*i.parse::<usize>().unwrap()-2](format!("input/input{}.txt", i)));
	println!("Day {} part 2 solution is {}", i, solutions[2*i.parse::<usize>().unwrap()-1](format!("input/input{}.txt", i)));
    }
}
